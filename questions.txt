Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
My favorite color is black.
***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
Steak!

3. Who is your favorite fictional character?
Goku

4. What is your favorite animal?
Dogs

5. What is your favorite programming language? (Hint: You can always say Python!!)
Python!
